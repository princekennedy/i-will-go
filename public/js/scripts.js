function initDataTable(el){


    // INITIALIZATION OF DATATABLES
    // =======================================================
    HSCore.components.HSDatatables.init($('#datatable'), {
      select: {
        style: 'multi',
        selector: 'td:first-child input[type="checkbox"]',
        classMap: {
          checkAll: '#datatableCheckAll',
          counter: '#datatableCounter',
          counterInfo: '#datatableCounterInfo'
        }
      },
      language: {
        zeroRecords: `<div class="text-center p-4">
              <img class="mb-3" src="./assets/svg/illustrations/oc-error.svg" alt="Image Description" style="width: 10rem;" data-hs-theme-appearance="default">
              <img class="mb-3" src="./assets/svg/illustrations-light/oc-error.svg" alt="Image Description" style="width: 10rem;" data-hs-theme-appearance="dark">
            <p class="mb-0">No data to show</p>
            </div>`
      }
    });

    const datatable = HSCore.components.HSDatatables.getItem(0)

    document.querySelectorAll('.js-datatable-filter').forEach(function (item) {
      item.addEventListener('change',function(e) {
        const elVal = e.target.value,
    targetColumnIndex = e.target.getAttribute('data-target-column-index'),
    targetTable = e.target.getAttribute('data-target-table');

    HSCore.components.HSDatatables.getItem(targetTable).column(targetColumnIndex).search(elVal !== 'null' ? elVal : '').draw()
      })
    })

  // =======================================================
  // HSCore.components.HSDatatables.init($(el), {
  //   dom: 'Bfrtip',
  //   buttons: [
  //     {
  //       extend: 'copy',
  //       className: 'd-none'
  //     },
  //     {
  //       extend: 'excel',
  //       className: 'd-none'
  //     },
  //     {
  //       extend: 'csv',
  //       className: 'd-none'
  //     },
  //     {
  //       extend: 'pdf',
  //       className: 'd-none'
  //     },
  //     {
  //       extend: 'print',
  //       className: 'd-none'
  //     },
  //   ],
  //   select: {
  //     style: 'multi',
  //     selector: 'td:first-child input[type="checkbox"]',
  //     classMap: {
  //       checkAll: '#datatableCheckAll',
  //       counter: '#datatableCounter',
  //       counterInfo: '#datatableCounterInfo'
  //     }
  //   },
  //   language: {
  //     zeroRecords: `
  //     <div class="text-center p-4">
  //       <img class="mb-3" src="/assets/svg/illustrations/oc-error.svg" alt="Image Description" style="width: 10rem;" data-hs-theme-appearance="default">
  //       <img class="mb-3" src="/assets/svg/illustrations-light/oc-error.svg" alt="Image Description" style="width: 10rem;" data-hs-theme-appearance="dark">
  //       <p class="mb-0">No data to show</p>
  //     </div>`
  //   }
  // })

  // const datatable = HSCore.components.HSDatatables.getItem(0)

  // $('#export-copy').click(function() {
  //   datatable.button('.buttons-copy').trigger()
  // });

  // $('#export-excel').click(function() {
  //   datatable.button('.buttons-excel').trigger()
  // });

  // $('#export-csv').click(function() {
  //   datatable.button('.buttons-csv').trigger()
  // });

  // $('#export-pdf').click(function() {
  //   datatable.button('.buttons-pdf').trigger()
  // });

  // $('#export-print').click(function() {
  //   datatable.button('.buttons-print').trigger()
  // });

  // $('.js-datatable-filter').on('change', function() {
  //   var $this = $(this),
  //     elVal = $this.val(),
  //     targetColumnIndex = $this.data('target-column-index');

  //   if (elVal === 'null') elVal = ''

  //   datatable.column(targetColumnIndex).search(elVal).draw();
  // });
}

$("document").ready( function() {
  // INITIALIZATION OF DATATABLES
  window.onload = function () {
    

    // INITIALIZATION OF NAVBAR VERTICAL ASIDE
    // =======================================================
    new HSSideNav('.js-navbar-vertical-aside').init()


    // INITIALIZATION OF FORM SEARCH
    // =======================================================
    new HSFormSearch('.js-form-search')


    // INITIALIZATION OF BOOTSTRAP DROPDOWN
    // =======================================================
    HSBsDropdown.init()


    // INITIALIZATION OF SELECT
    // =======================================================
    HSCore.components.HSTomSelect.init('.js-select')


    // INITIALIZATION OF INPUT MASK
    // =======================================================
    HSCore.components.HSMask.init('.js-input-mask')


    // INITIALIZATION OF NAV SCROLLER
    // =======================================================
    new HsNavScroller('.js-nav-scroller')


    // INITIALIZATION OF COUNTER
    // =======================================================
    new HSCounter('.js-counter')


    // INITIALIZATION OF TOGGLE PASSWORD
    // =======================================================
    new HSTogglePassword('.js-toggle-password')


    // INITIALIZATION OF FILE ATTACHMENT
    // =======================================================
    new HSFileAttach('.js-file-attach')
  }

  // STYLE SWITCHER
  // =======================================================
  const $dropdownBtn = document.getElementById('selectThemeDropdown') // Dropdowon trigger
  const $variants = document.querySelectorAll(`[aria-labelledby="selectThemeDropdown"] [data-icon]`) // All items of the dropdown

  // Function to set active style in the dorpdown menu and set icon for dropdown trigger
  const setActiveStyle = function () {
    $variants.forEach($item => {
      if ($item.getAttribute('data-value') === HSThemeAppearance.getOriginalAppearance()) {
        $dropdownBtn.innerHTML = `<i class="${$item.getAttribute('data-icon')}" />`
        return $item.classList.add('active')
      }

      $item.classList.remove('active')
    })
  }

  // Add a click event to all items of the dropdown to set the style
  $variants.forEach(function ($item) {
    $item.addEventListener('click', function () {
      HSThemeAppearance.setAppearance($item.getAttribute('data-value'))
    })
  })

  // Call the setActiveStyle on load page
  setActiveStyle()

  // Add event listener on change style to call the setActiveStyle function
  window.addEventListener('on-hs-appearance-change', function () {
    setActiveStyle()
  })

});