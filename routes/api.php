<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\TrackController;
use App\Http\Controllers\Api\ChurchController;
use App\Http\Controllers\Api\StatusController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\UserUsersController;
use App\Http\Controllers\Api\UserTracksController;
use App\Http\Controllers\Api\PermissionController;
use App\Http\Controllers\Api\ChurchUsersController;
use App\Http\Controllers\Api\StatusUsersController;
use App\Http\Controllers\Api\StatusTracksController;
use App\Http\Controllers\Api\UserStatusesController;
use App\Http\Controllers\Api\UserChurchesController;
use App\Http\Controllers\Api\CategoryTracksController;
use App\Http\Controllers\Api\StatusChurchesController;
use App\Http\Controllers\Api\UserCategoriesController;
use App\Http\Controllers\Api\StatusCategoriesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [AuthController::class, 'login'])->name('api.login');

Route::middleware('auth:sanctum')
    ->get('/user', function (Request $request) {
        return $request->user();
    })
    ->name('api.user');

Route::name('api.')
    ->middleware('auth:sanctum')
    ->group(function () {
        Route::apiResource('roles', RoleController::class);
        Route::apiResource('permissions', PermissionController::class);

        Route::apiResource('categories', CategoryController::class);

        // Category Tracks
        Route::get('/categories/{category}/tracks', [
            CategoryTracksController::class,
            'index',
        ])->name('categories.tracks.index');
        Route::post('/categories/{category}/tracks', [
            CategoryTracksController::class,
            'store',
        ])->name('categories.tracks.store');

        Route::apiResource('churches', ChurchController::class);

        // Church Users
        Route::get('/churches/{church}/users', [
            ChurchUsersController::class,
            'index',
        ])->name('churches.users.index');
        Route::post('/churches/{church}/users', [
            ChurchUsersController::class,
            'store',
        ])->name('churches.users.store');

        Route::apiResource('statuses', StatusController::class);

        // Status Users
        Route::get('/statuses/{status}/users', [
            StatusUsersController::class,
            'index',
        ])->name('statuses.users.index');
        Route::post('/statuses/{status}/users', [
            StatusUsersController::class,
            'store',
        ])->name('statuses.users.store');

        // Status Tracks
        Route::get('/statuses/{status}/tracks', [
            StatusTracksController::class,
            'index',
        ])->name('statuses.tracks.index');
        Route::post('/statuses/{status}/tracks', [
            StatusTracksController::class,
            'store',
        ])->name('statuses.tracks.store');

        // Status Churches
        Route::get('/statuses/{status}/churches', [
            StatusChurchesController::class,
            'index',
        ])->name('statuses.churches.index');
        Route::post('/statuses/{status}/churches', [
            StatusChurchesController::class,
            'store',
        ])->name('statuses.churches.store');

        // Status Categories
        Route::get('/statuses/{status}/categories', [
            StatusCategoriesController::class,
            'index',
        ])->name('statuses.categories.index');
        Route::post('/statuses/{status}/categories', [
            StatusCategoriesController::class,
            'store',
        ])->name('statuses.categories.store');

        Route::apiResource('tracks', TrackController::class);

        Route::apiResource('users', UserController::class);

        // User Tracks
        Route::get('/users/{user}/tracks', [
            UserTracksController::class,
            'index',
        ])->name('users.tracks.index');
        Route::post('/users/{user}/tracks', [
            UserTracksController::class,
            'store',
        ])->name('users.tracks.store');

        // User Categories
        Route::get('/users/{user}/categories', [
            UserCategoriesController::class,
            'index',
        ])->name('users.categories.index');
        Route::post('/users/{user}/categories', [
            UserCategoriesController::class,
            'store',
        ])->name('users.categories.store');

        // User Categories2
        Route::get('/users/{user}/categories', [
            UserCategoriesController::class,
            'index',
        ])->name('users.categories.index');
        Route::post('/users/{user}/categories', [
            UserCategoriesController::class,
            'store',
        ])->name('users.categories.store');

        // User Users
        Route::get('/users/{user}/users', [
            UserUsersController::class,
            'index',
        ])->name('users.users.index');
        Route::post('/users/{user}/users', [
            UserUsersController::class,
            'store',
        ])->name('users.users.store');

        // User Users2
        Route::get('/users/{user}/users', [
            UserUsersController::class,
            'index',
        ])->name('users.users.index');
        Route::post('/users/{user}/users', [
            UserUsersController::class,
            'store',
        ])->name('users.users.store');

        // User Tracks2
        Route::get('/users/{user}/tracks', [
            UserTracksController::class,
            'index',
        ])->name('users.tracks.index');
        Route::post('/users/{user}/tracks', [
            UserTracksController::class,
            'store',
        ])->name('users.tracks.store');

        // User Tracks3
        Route::get('/users/{user}/tracks', [
            UserTracksController::class,
            'index',
        ])->name('users.tracks.index');
        Route::post('/users/{user}/tracks', [
            UserTracksController::class,
            'store',
        ])->name('users.tracks.store');

        // User Statuses
        Route::get('/users/{user}/statuses', [
            UserStatusesController::class,
            'index',
        ])->name('users.statuses.index');
        Route::post('/users/{user}/statuses', [
            UserStatusesController::class,
            'store',
        ])->name('users.statuses.store');

        // User Statuses2
        Route::get('/users/{user}/statuses', [
            UserStatusesController::class,
            'index',
        ])->name('users.statuses.index');
        Route::post('/users/{user}/statuses', [
            UserStatusesController::class,
            'store',
        ])->name('users.statuses.store');

        // User Churches
        Route::get('/users/{user}/churches', [
            UserChurchesController::class,
            'index',
        ])->name('users.churches.index');
        Route::post('/users/{user}/churches', [
            UserChurchesController::class,
            'store',
        ])->name('users.churches.store');

        // User Churches2
        Route::get('/users/{user}/churches', [
            UserChurchesController::class,
            'index',
        ])->name('users.churches.index');
        Route::post('/users/{user}/churches', [
            UserChurchesController::class,
            'store',
        ])->name('users.churches.store');
    });
