<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use App\Models\Scopes\Searchable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Traits\CreatedUpdatedTrait;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;


class User extends Authenticatable implements HasMedia
{
    use HasRoles;
    use Notifiable;
    use HasFactory;
    use Searchable;
    use SoftDeletes;
    use HasApiTokens;
    use CreatedUpdatedTrait;
    use InteractsWithMedia;

    protected $fillable = [
        'name',
        'email',
        'title',
        'phone',
        'description',
        'password',
        'status_id',
        'created_by',
        'updated_by',
        'church_id',
    ];

    protected $searchableFields = ['*'];

    protected $hidden = ['password', 'remember_token'];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $appends = ['image'];

    public function getImageAttribute(){
        $file = $this->file();
        return $file ? $file->getUrl() : '/img/bg-img/41.jpg';
    }

    public function file($collection = "image"){
        return $this->getMedia($collection)->first();
    }


    public function tracks()
    {
        return $this->hasMany(Track::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function categories()
    {
        return $this->hasMany(Category::class, 'created_by');
    }

    public function categories2()
    {
        return $this->hasMany(Category::class, 'updated_by');
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function users2()
    {
        return $this->hasMany(User::class, 'updated_by');
    }

    public function tracks2()
    {
        return $this->hasMany(Track::class, 'created_by');
    }

    public function tracks3()
    {
        return $this->hasMany(Track::class, 'updated_by');
    }

    public function statuses()
    {
        return $this->hasMany(Status::class, 'created_by');
    }

    public function statuses2()
    {
        return $this->hasMany(Status::class, 'updated_by');
    }

    public function church()
    {
        return $this->belongsTo(Church::class);
    }

    public function churches()
    {
        return $this->hasMany(Church::class, 'created_by');
    }

    public function churches2()
    {
        return $this->hasMany(Church::class, 'updated_by');
    }
    public function isSuperAdmin()
    {
        return $this->hasRole('super-admin');
    }
}
