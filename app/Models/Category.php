<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\CreatedUpdatedTrait;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Category extends Model
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;
    use CreatedUpdatedTrait;

    protected $fillable = [
        'name',
        'description',
        'created_by',
        'updated_by',
        'status_id',
    ];

    protected $searchableFields = ['*'];

    public function tracks()
    {
        return $this->hasMany(Track::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
