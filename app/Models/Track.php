<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\CreatedUpdatedTrait;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Track extends Model implements HasMedia
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;
    use InteractsWithMedia;
    use CreatedUpdatedTrait;

    protected $fillable = [
        'name',
        'user_id',
        'category_id',
        'description',
        'status_id',
        'created_by',
        'updated_by',
    ];


    protected $appends = ['url'];

    public function getUrlAttribute(){
        $file = $this->file();
        return $file ? $file->getUrl() : '';
    }

    public function file($collection = "media"){
        return $this->getMedia($collection)->first();
    }


    protected $searchableFields = ['*'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
