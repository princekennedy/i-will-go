<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\CreatedUpdatedTrait;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Church extends Model implements HasMedia
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;
    use InteractsWithMedia;
    use CreatedUpdatedTrait;

    protected $fillable = [
        'name',
        'description',
        'status_id',
        'created_by',
        'updated_by',
    ];

    protected $searchableFields = ['*'];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }
}
