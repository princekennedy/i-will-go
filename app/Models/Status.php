<?php

namespace App\Models;

use App\Models\Scopes\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Traits\CreatedUpdatedTrait;

class Status extends Model
{
    use HasFactory;
    use Searchable;
    use SoftDeletes;
    use CreatedUpdatedTrait;

    protected $fillable = [
        'name',
        'color',
        'description',
        'created_by',
        'updated_by',
    ];

    protected $searchableFields = ['*'];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function tracks()
    {
        return $this->hasMany(Track::class);
    }

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function churches()
    {
        return $this->hasMany(Church::class);
    }

    public function categories()
    {
        return $this->hasMany(Category::class);
    }
}
