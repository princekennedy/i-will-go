<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Facades\App\Cache\Users;

trait BranchTrait
{


    public static function bootBranchTrait()
    {
        

        if (auth()->check() ) {

            static::addGlobalScope(function (Builder $builder) {
            	// $reports = \App\Models\Report::where("function_type", "Hidden")->where("branch_id", auth()->user()->branch_id)->pluck('columns');
            	// foreach ($reports as $column) {
             //        // dd($column);
            	// 	$builder->whereNotIn("id", _array($column) );
            	// }
            	
                // $builder->whereHas('roles', function($q) {
                //     $q->whereIn('id', auth()->user()->roles);
                // }); //->orWhereNull($field);
            });            

            static::creating(function ($model) {
		        $model->branch_id = auth()->user()->branch_id;
		    });

      //       static::updating(function ($model) {
		    //     $model->updated_by = auth()->id();
		    // });

      //       static::deleting(function ($model) {
      //           $model->updated_by = auth()->id();
      //       });

        }
    }

}
