<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Facades\App\Cache\Users;

  
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use DB;
  

trait FormTrait
{


    public static function bootFormTrait()
    {

        if (auth()->check() && !auth()->user()->isSuperAdmin()) {

            // static::addGlobalScope(function (Builder $builder) {
            //     $builder->whereHas('roles', function($q) {
            //         $q->whereIn('id', auth()->user()->roles->pluck('id'));
            //     }); //->orWhereNull($field);
            // });            


      //       static::creating(function ($model) {
		    //     $model->created_by = auth()->id();
		    // });

      //       static::updating(function ($model) {
		    //     $model->updated_by = auth()->id();
		    // });

      //       static::deleting(function ($model) {
      //           $model->updated_by = auth()->id();
      //       });

        }
    }

    public function migrateTable(){
        $form = $this;
        $tableName = "form_" . $form->id;
        
        if(Schema::hasTable($tableName) && ! DB::table($tableName)->count()){
            Schema::dropIfExists($tableName);
        }

        if (!Schema::hasTable($tableName)) {
            Schema::create( $tableName , function (Blueprint $table) use ($form) {
                $table->bigIncrements('id');
                $table->string('department')->nullable();
                $table->string('position')->nullable();
                $table->string('branch')->nullable();
                $table->string('user')->nullable();
                $table->text('body')->nullable();
                $table->unsignedInteger('published_by')->nullable();
                $table->unsignedInteger('unique_value')->nullable();

                $table->unsignedInteger('created_by')->nullable();
                $table->unsignedInteger('updated_by')->nullable();
                $table->timestamps();
                $table->softDeletes();
            });
        }

        foreach ($form->inputs as $input) {
            $column = "column_" . $input->id;
            if(!Schema::hasColumn($tableName, $column)) {
                Schema::table( $tableName , function (Blueprint $table) use ($input, $column) {
                    switch ($input->type) {
                        case 'date':
                            $table->date($column)->nullable();
                            break;                    
                        case 'datetime':
                            $table->datetime($column)->nullable();
                            break;
                        
                        case 'number':
                            $table->integer($column)->nullable();
                            break;
                        
                        default:
                            $table->text($column)->nullable();
                            break;
                    }
                });
            }
            
        }


    }

}
