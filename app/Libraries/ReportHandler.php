<?php 

namespace App\Libraries;

use App\Models\User;
use App\Models\Form;
use App\Models\Input;
use App\Models\FormResponse;
use App\Models\InputResponse;
use Carbon\Carbon;
use DB;

/**
 * 
 */
class ReportHandler
{

	private $reports = [
		'list' => ['name' => 'LIST', 'types' => []],
		'average' => ['name' => 'AVERAGE', 'types' => ['number', 'currency']],
		'sum'     => ['name' => 'SUM'    , 'types' => ['number', 'currency']],
		'count'   => ['name' => 'COUNT'  , 'types' => []],
	];	

	private $groupByInput = null;
	private $groupByKey   = null;

	public function reportList(){
		return $this->reports;
	}

    public function dateFrom(){
        return  request()->start_date ?? Carbon::now()->subDays(90)->format('Y-m-d');
    }

    public function dateTo(){
        return request()->end_date ?? Carbon::now()->addDays(1)->format('Y-m-d');
    }

	public function applyDates($builder){
		$builder->where('created_at', '>=', $this->dateFrom());
		$builder->where('created_at', '<=', $this->dateTo());
		return $builder;
	}

	public function list( $form, $report ){
        $data = new \App\Exports\AnswerExport($form, $report);
		// $formResponses = $this->groupBy($formResponses, $report);
        return $data->array();
	}

	public function average( $form, $report ){
		$formResponses = $this->applyDates( $form->FormInstance() );
		// $formResponses->whereHas("inputResponses", function($q) use ($report) { $q->whereIn("input_id", _array($report->columns)); });
		// $formResponses = $formResponses->get();
		// $formResponses = $this->groupBy($formResponses, $report);
		$data = [];
		foreach( $report->inputs as $input){
			$columnName = "column_" . $input->id;
			$value = $formResponses->avg( $columnName );
			$value = _forceNumber( $value );
			$data[] = [
				"columnName" => $input->name,
				"columnValue" => $value,
			];
		}
		return $data;
	}

	public function sum( $form, $report ){
		$formResponses = $this->applyDates( $form->FormInstance() );
		// $formResponses->whereHas("inputResponses", function($q) use ($report) { $q->whereIn("input_id", _array($report->columns)); });
		// $formResponses = $formResponses->get();
		// $formResponses = $this->groupBy($formResponses, $report);
		$data = [];
		foreach( $report->inputs as $input){
			$columnName = "column_" . $input->id;
			$value = $formResponses->sum( $columnName );
			$value = _forceNumber( $value );
			$data[] = [
				"columnName" => $input->name,
				"columnValue" => $value,
			];
		}
		return $data;
	}

	public function count( $form, $report ){
		$formResponses = $this->applyDates( $form->FormInstance() );
		// $formResponses->whereHas("inputResponses", function($q) use ($report) { $q->whereIn("input_id", _array($report->columns)); });
		// $formResponses = $formResponses->get();
		// $formResponses = $this->groupBy($formResponses, $report);
		$data = [];
		foreach( $report->inputs as $input){
			$columnName = "column_" . $input->id;
			$count = $formResponses->groupBy( $columnName );
			$value = $count->count();
			$data[] = [
				"columnName" => $input->name,
				"columnValue" => $value,
			];
		}
		return $data;
	}

	public function hidden( $form, $report ){
		return $report->inputs->pluck("name");
	}

	public function groupBy($builder, $report){

		$groupBy = $report->group_by;

		if($groupBy == 'day'){
			$builder = $builder->groupBy(function($val) { return Carbon::parse($val->created_at)->format('Y-m-d'); });
		} else if ($groupBy == 'month' ) {
			$builder = $builder->groupBy(function($val) { return Carbon::parse($val->created_at)->format('Y-m'); });
		} else if ($groupBy == 'year' ) {
			$builder = $builder->groupBy(function($val) { return Carbon::parse($val->created_at)->format('Y'); });
		} else if ($groupBy == 'date' ) {
			$builder = $builder->groupBy(function($val) { return Carbon::parse($val->created_at)->format('Y-m-d H:i:s'); });
		} else if ($groupBy == 'user' ) {
			$builder = $builder->groupBy(function($val) { return User::find($val->created_by)->name ?? '-'; });
		} else if ($groupBy == 'item' ) {
			$builder = $builder->groupBy(function($val) { return Input::find($val->input_id)->name ?? '-'; });
		} else {
			$builder = [];
		}
		return $builder;
	}

	public function getReports($form = null){
		$data  = [];
		if( !$form ) return [];
		$reports = $form->reports()->orderBy('name', 'ASC')->get();
		foreach( $reports as $report){
			$data[] = [
				"report" => $report,
				"data" => call_user_func_array( [ $this, \Str::lower($report->function_type) ], [ $form, $report ] ),
			];
		}
		return $data;
	}

	public function getReport($report){
		return [
			"data" => call_user_func_array( [ $this, \Str::lower($report->function_type) ], [ $report->form, $report ] )
		];
	}

}