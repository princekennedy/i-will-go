<?php
/*
* flash array message
* params( array| json string| object)
* return object
*/
if(!method_exists($this, '_object')){
        function _object($array){
                if(is_array($array)) return (object) $array;
                if(is_object($array)) return $array;
                if(is_string($array)) return (object) json_decode($array, true);
        return $array; 
        }
}
/*
* flash array message
* params( array| json string| object)
* return array
*/
if(!method_exists($this, '_array')){
        function _array($array){
                if(is_object($array)) return (array) $array;
                if(is_array($array)) return $array;
                if(is_string($array)) return (array) json_decode($array, true);
                return $array; 
        }
}
/*
* extract value from array, object
* params( array|object, key)
* return value
*/
if(!method_exists($this, '_from')){
        function _from($object, $key){
                $object = _array($object);
                $value = (is_array($object) && array_key_exists($key, $object)) ? $object[$key] : "" ; 
                return $value; 
        }
}
/*
* flash array message
* params( array= ['error' => 'Error'])
* return void
*/
if(!method_exists($this, '_flash')){
        function _flash($object){
                $array = _array($object);
                foreach ($array as $key => $value) {
                        session()->flash( $key , $value);
                }
        }
}
/*
* flash array message
* params( array= ['error' => 'Error'])
* return void
*/
if(!method_exists($this, '_money')){
        function _money($number){
                return "MK " . _number($number);
        }
}

/*
* flash array message
* params( array= ['error' => 'Error'])
* return void
*/
if(!method_exists($this, '_number')){
        function _number($number){
                if(is_numeric($number)) return number_format($number);
                if($number >= 0) return number_format($number);
                return 0;
        }
}

/*
* flash array message
* params( array= ['error' => 'Error'])
* return void
*/
if(!method_exists($this, '_forceNumber')){
        function _forceNumber($number){
                // Being sure the string is actually a number
                if (is_numeric($number))
                    $number = $number + 0;
                else // Let the number be 0 if the string is not a number
                    $number = 0;
                return $number;
        }
}


/*
* flash array message
* params( string)
* return array
*/
if(!method_exists($this, '_formatText')){
        function _formatText($str){
                if(is_string($str)) return ucfirst( str_replace("_", " ", $str));
                return ""; 
        }
}
/*
* flash array message
* params( string)
* return array
*/
if(!method_exists($this, '_keyText')){
        function _keyText($str){
                if(is_string($str)) return ucfirst( str_replace(" ", "_", $str));
                return "0"; 
        }
}

/*
* flash array message
* params( string)
* return array
*/
if(!method_exists($this, '_explode')){
        function _explode($str, $delimiter = ","){
                if(is_string($str)) return explode($delimiter, $str);
                return []; 
        }
}

/*
* flash array message
* params( string)
* return array
*/
if(!method_exists($this, '_implode')){
        function _implode($arr, $glue = ","){
                if(is_array($arr)) return implode($glue, $arr);
                if(is_string($arr)) return $arr;
                return ""; 
        }
}

/*
* flash array message
* params( string)
* return array
*/
if(!method_exists($this, '_pad')){
        function _pad($str, $num = 5,$by, $dir = STR_PAD_LEFT){
                return str_pad($str, $num, $by, $dir);
        }
}

/*
* flash array message
* params( string)
* return array
*/
if(!method_exists($this, '_user')){
        function _user(){
                return cache(env('APP_NAME') . request()->ip());
        }
}