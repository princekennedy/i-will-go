<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Track;
use Illuminate\Auth\Access\HandlesAuthorization;

class TrackPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the track can view any models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return $user->hasPermissionTo('list tracks');
    }

    /**
     * Determine whether the track can view the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Track  $model
     * @return mixed
     */
    public function view(User $user, Track $model)
    {
        return $user->hasPermissionTo('view tracks');
    }

    /**
     * Determine whether the track can create models.
     *
     * @param  App\Models\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->hasPermissionTo('create tracks');
    }

    /**
     * Determine whether the track can update the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Track  $model
     * @return mixed
     */
    public function update(User $user, Track $model)
    {
        return $user->hasPermissionTo('update tracks');
    }

    /**
     * Determine whether the track can delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Track  $model
     * @return mixed
     */
    public function delete(User $user, Track $model)
    {
        return $user->hasPermissionTo('delete tracks');
    }

    /**
     * Determine whether the user can delete multiple instances of the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Track  $model
     * @return mixed
     */
    public function deleteAny(User $user)
    {
        return $user->hasPermissionTo('delete tracks');
    }

    /**
     * Determine whether the track can restore the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Track  $model
     * @return mixed
     */
    public function restore(User $user, Track $model)
    {
        return false;
    }

    /**
     * Determine whether the track can permanently delete the model.
     *
     * @param  App\Models\User  $user
     * @param  App\Models\Track  $model
     * @return mixed
     */
    public function forceDelete(User $user, Track $model)
    {
        return false;
    }
}
