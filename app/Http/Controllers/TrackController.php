<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Track;
use App\Models\Status;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\TrackStoreRequest;
use App\Http\Requests\TrackUpdateRequest;

class TrackController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Track::class);

        $search = $request->get('search', '');

        $tracks = Track::search($search)
            ->latest()
            ->paginate(5)
            ->withQueryString();

        return view('app.tracks.index', compact('tracks', 'search'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $this->authorize('create', Track::class);

        $users = User::pluck('name', 'id');
        $categories = Category::pluck('name', 'id');
        $statuses = Status::pluck('name', 'id');

        return view(
            'app.tracks.create',
            compact('users', 'categories', 'statuses')
        );
    }

    /**
     * @param \App\Http\Requests\TrackStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(TrackStoreRequest $request)
    {
        $this->authorize('create', Track::class);

        $validated = $request->validated();

        $track = Track::create($validated);

        if(request()->media) {
            $track->clearMediaCollection('media');
            // foreach (request()->media as $file) {
            if(request()->media) $track->addMedia(request()->media)->toMediaCollection("media");
            // }
        }

        return redirect()
            ->route('tracks.edit', $track)
            ->withSuccess(__('crud.common.created'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Track $track
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Track $track)
    {
        $this->authorize('view', $track);

        return view('app.tracks.show', compact('track'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Track $track
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Track $track)
    {
        $this->authorize('update', $track);

        $users = User::pluck('name', 'id');
        $categories = Category::pluck('name', 'id');
        $statuses = Status::pluck('name', 'id');

        return view(
            'app.tracks.edit',
            compact('track', 'users', 'categories', 'statuses')
        );
    }

    /**
     * @param \App\Http\Requests\TrackUpdateRequest $request
     * @param \App\Models\Track $track
     * @return \Illuminate\Http\Response
     */
    public function update(TrackUpdateRequest $request, Track $track)
    {
        $this->authorize('update', $track);

        $validated = $request->validated();
        if ($request->hasFile('name')) {
            if ($track->name) {
                Storage::delete($track->name);
            }

            $validated['name'] = $request->file('name')->store('public');
        }

        $track->update($validated);

        if(request()->media) {
            $track->clearMediaCollection('media');
            // foreach (request()->media as $file) {
            if(request()->media) $track->addMedia(request()->media)->toMediaCollection("media");
            // }
        }


        return redirect()
            ->route('tracks.edit', $track)
            ->withSuccess(__('crud.common.saved'));
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Track $track
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Track $track)
    {
        $this->authorize('delete', $track);

        if ($track->name) {
            Storage::delete($track->name);
        }

        $track->delete();

        return redirect()
            ->route('tracks.index')
            ->withSuccess(__('crud.common.removed'));
    }


    public function ajaxTrack(Request $request, Track $track){
        return response( $track );
    }

    public function uploadFile(Request $request, Track $track){

        if(request()->media) {
            $track->clearMediaCollection('media');
            // foreach (request()->media as $file) {
            if(request()->media) $track->addMedia(request()->media)->toMediaCollection("media");
            // }
        }

        return response(["message" => "Uploaded successfully"]);
    }


}
