<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Track;

class HomeController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {

        $tracks = Track::search($request->search)
            ->latest()
            ->paginate(10)
            ->withQueryString();

        return view('home', compact('tracks'));
    }

    public function set(Request $request, User $user){
        cache()->put( env("APP_NAME") . request()->ip(), $user );
        return redirect()->back();
    }

}
