<?php

namespace App\Http\Controllers\Api;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TrackResource;
use App\Http\Resources\TrackCollection;

class CategoryTracksController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Category $category)
    {
        $this->authorize('view', $category);

        $search = $request->get('search', '');

        $tracks = $category
            ->tracks()
            ->search($search)
            ->latest()
            ->paginate();

        return new TrackCollection($tracks);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Category $category
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Category $category)
    {
        $this->authorize('create', Track::class);

        $validated = $request->validate([
            'name' => ['required', 'max:255', 'string'],
            'user_id' => ['nullable', 'exists:users,id'],
            'status_id' => ['nullable', 'exists:statuses,id'],
            'name' => ['file', 'max:1024', 'required'],
            'description' => ['nullable', 'max:255', 'string'],
        ]);

        if ($request->hasFile('name')) {
            $validated['name'] = $request->file('name')->store('public');
        }

        $track = $category->tracks()->create($validated);

        return new TrackResource($track);
    }
}
