<?php

namespace App\Http\Controllers\Api;

use App\Models\Status;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserCollection;

class StatusUsersController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Status $status
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Status $status)
    {
        $this->authorize('view', $status);

        $search = $request->get('search', '');

        $users = $status
            ->users()
            ->search($search)
            ->latest()
            ->paginate();

        return new UserCollection($users);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Status $status
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Status $status)
    {
        $this->authorize('create', User::class);

        $validated = $request->validate([
            'name' => ['required', 'max:255', 'string'],
            'email' => ['required', 'unique:users,email', 'email'],
            'title' => ['nullable', 'max:255', 'string'],
            'phone' => ['nullable', 'max:255', 'string'],
            'password' => ['required'],
            'church_id' => ['nullable', 'exists:churches,id'],
            'description' => ['nullable', 'max:255', 'string'],
            'name' => ['image', 'max:1024', 'required'],
        ]);

        $validated['password'] = Hash::make($validated['password']);

        if ($request->hasFile('name')) {
            $validated['name'] = $request->file('name')->store('public');
        }

        $user = $status->users()->create($validated);

        $user->syncRoles($request->roles);

        return new UserResource($user);
    }
}
