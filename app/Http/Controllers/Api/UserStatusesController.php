<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\StatusResource;
use App\Http\Resources\StatusCollection;

class UserStatusesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        $this->authorize('view', $user);

        $search = $request->get('search', '');

        $statuses = $user
            ->statuses2()
            ->search($search)
            ->latest()
            ->paginate();

        return new StatusCollection($statuses);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $this->authorize('create', Status::class);

        $validated = $request->validate([
            'name' => ['required', 'max:255', 'string'],
            'color' => ['required'],
            'description' => ['nullable', 'max:255', 'string'],
        ]);

        $status = $user->statuses2()->create($validated);

        return new StatusResource($status);
    }
}
