<?php

namespace App\Http\Controllers\Api;

use App\Models\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TrackResource;
use App\Http\Resources\TrackCollection;

class StatusTracksController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Status $status
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Status $status)
    {
        $this->authorize('view', $status);

        $search = $request->get('search', '');

        $tracks = $status
            ->tracks()
            ->search($search)
            ->latest()
            ->paginate();

        return new TrackCollection($tracks);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Status $status
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Status $status)
    {
        $this->authorize('create', Track::class);

        $validated = $request->validate([
            'name' => ['required', 'max:255', 'string'],
            'user_id' => ['nullable', 'exists:users,id'],
            'category_id' => ['nullable', 'exists:categories,id'],
            'name' => ['file', 'max:1024', 'required'],
            'description' => ['nullable', 'max:255', 'string'],
        ]);

        if ($request->hasFile('name')) {
            $validated['name'] = $request->file('name')->store('public');
        }

        $track = $status->tracks()->create($validated);

        return new TrackResource($track);
    }
}
