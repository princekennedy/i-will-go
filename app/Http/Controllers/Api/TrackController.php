<?php

namespace App\Http\Controllers\Api;

use App\Models\Track;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TrackResource;
use App\Http\Resources\TrackCollection;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\TrackStoreRequest;
use App\Http\Requests\TrackUpdateRequest;

class TrackController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $this->authorize('view-any', Track::class);

        $search = $request->get('search', '');

        $tracks = Track::search($search)
            ->latest()
            ->paginate();

        return new TrackCollection($tracks);
    }

    /**
     * @param \App\Http\Requests\TrackStoreRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(TrackStoreRequest $request)
    {
        $this->authorize('create', Track::class);

        $validated = $request->validated();
        if ($request->hasFile('name')) {
            $validated['name'] = $request->file('name')->store('public');
        }

        $track = Track::create($validated);

        return new TrackResource($track);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Track $track
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, Track $track)
    {
        $this->authorize('view', $track);

        return new TrackResource($track);
    }

    /**
     * @param \App\Http\Requests\TrackUpdateRequest $request
     * @param \App\Models\Track $track
     * @return \Illuminate\Http\Response
     */
    public function update(TrackUpdateRequest $request, Track $track)
    {
        $this->authorize('update', $track);

        $validated = $request->validated();

        if ($request->hasFile('name')) {
            if ($track->name) {
                Storage::delete($track->name);
            }

            $validated['name'] = $request->file('name')->store('public');
        }

        $track->update($validated);

        return new TrackResource($track);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Track $track
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Track $track)
    {
        $this->authorize('delete', $track);

        if ($track->name) {
            Storage::delete($track->name);
        }

        $track->delete();

        return response()->noContent();
    }
}
