<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ChurchResource;
use App\Http\Resources\ChurchCollection;

class UserChurchesController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        $this->authorize('view', $user);

        $search = $request->get('search', '');

        $churches = $user
            ->churches2()
            ->search($search)
            ->latest()
            ->paginate();

        return new ChurchCollection($churches);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $this->authorize('create', Church::class);

        $validated = $request->validate([
            'name' => ['required', 'max:255', 'string'],
            'status_id' => ['nullable', 'exists:statuses,id'],
            'description' => ['nullable', 'max:255', 'string'],
        ]);

        $church = $user->churches2()->create($validated);

        return new ChurchResource($church);
    }
}
