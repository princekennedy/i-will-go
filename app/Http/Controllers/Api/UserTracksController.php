<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\TrackResource;
use App\Http\Resources\TrackCollection;

class UserTracksController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, User $user)
    {
        $this->authorize('view', $user);

        $search = $request->get('search', '');

        $tracks = $user
            ->tracks3()
            ->search($search)
            ->latest()
            ->paginate();

        return new TrackCollection($tracks);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        $this->authorize('create', Track::class);

        $validated = $request->validate([
            'name' => ['required', 'max:255', 'string'],
            'category_id' => ['nullable', 'exists:categories,id'],
            'status_id' => ['nullable', 'exists:statuses,id'],
            'name' => ['file', 'max:1024', 'required'],
            'description' => ['nullable', 'max:255', 'string'],
        ]);

        if ($request->hasFile('name')) {
            $validated['name'] = $request->file('name')->store('public');
        }

        $track = $user->tracks3()->create($validated);

        return new TrackResource($track);
    }
}
