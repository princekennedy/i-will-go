<?php

namespace App\Http\Controllers\Api;

use App\Models\Church;
use Illuminate\Http\Request;
use App\Http\Resources\UserResource;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\UserCollection;

class ChurchUsersController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Church $church
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Church $church)
    {
        $this->authorize('view', $church);

        $search = $request->get('search', '');

        $users = $church
            ->users()
            ->search($search)
            ->latest()
            ->paginate();

        return new UserCollection($users);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Church $church
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Church $church)
    {
        $this->authorize('create', User::class);

        $validated = $request->validate([
            'name' => ['required', 'max:255', 'string'],
            'email' => ['required', 'unique:users,email', 'email'],
            'title' => ['nullable', 'max:255', 'string'],
            'phone' => ['nullable', 'max:255', 'string'],
            'password' => ['required'],
            'status_id' => ['nullable', 'exists:statuses,id'],
            'description' => ['nullable', 'max:255', 'string'],
            'name' => ['image', 'max:1024', 'required'],
        ]);

        $validated['password'] = Hash::make($validated['password']);

        if ($request->hasFile('name')) {
            $validated['name'] = $request->file('name')->store('public');
        }

        $user = $church->users()->create($validated);

        $user->syncRoles($request->roles);

        return new UserResource($user);
    }
}
