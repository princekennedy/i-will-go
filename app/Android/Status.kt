package com.example


/**
* Homeflow Technologies | Status.
*
* @property id
* @property name
* @property color
* @property description
* @property created_by
* @property updated_by
*
* @constructor Create Status model
*/
data class Status( var name: String? = null, var color: String? = null, var description: String? = null, var created_by: Int? = null, var updated_by: Int? = null,
)
