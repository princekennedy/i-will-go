package com.example


/**
* Homeflow Technologies | User.
*
* @property id
* @property name
* @property email
* @property title
* @property phone
* @property description
* @property password
* @property status_id
* @property created_by
* @property updated_by
* @property church_id
*
* @constructor Create User model
*/
data class User( var name: String? = null, var email: String? = null, var title: String? = null, var phone: String? = null, var description: String? = null, var password: String? = null, var status_id: Int? = null, var created_by: Int? = null, var updated_by: Int? = null, var church_id: Int? = null,
)
