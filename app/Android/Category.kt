package com.example


/**
* Homeflow Technologies | Category.
*
* @property id
* @property name
* @property description
* @property created_by
* @property updated_by
* @property status_id
*
* @constructor Create Category model
*/
data class Category( var name: String? = null, var description: String? = null, var created_by: Int? = null, var updated_by: Int? = null, var status_id: Int? = null,
)
