package com.example


/**
* Homeflow Technologies | Track.
*
* @property id
* @property name
* @property user_id
* @property category_id
* @property description
* @property status_id
* @property created_by
* @property updated_by
*
* @constructor Create Track model
*/
data class Track( var name: String? = null, var user_id: Int? = null, var category_id: Int? = null, var description: String? = null, var status_id: Int? = null, var created_by: Int? = null, var updated_by: Int? = null,
)
