package com.example


/**
* Homeflow Technologies | Church.
*
* @property id
* @property name
* @property description
* @property status_id
* @property created_by
* @property updated_by
*
* @constructor Create Church model
*/
data class Church( var name: String? = null, var description: String? = null, var status_id: Int? = null, var created_by: Int? = null, var updated_by: Int? = null,
)
