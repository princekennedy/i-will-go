@extends('layouts.app')

@section('content')


    <!-- ##### Hero Area Start ##### -->
    <section class="hero--area section-padding-80" id="home-el">
        <div class="container">
            <div class="row no-gutters">


                <div class="col-12 col-md-5 col-lg-4">


                    <ul class="nav vizew-nav-tab" role="tablist">

                        @forelse( $tracks as $track)

                        <li class="nav-item" style="width: 100%;">
                            <a class="nav-link active" @click="getTrack('{{ $track->id }}')" id="post-1-tab" data-toggle="pill" href="#post-1" role="tab" aria-controls="post-1" aria-selected="true">
                                <!-- Single Blog Post -->
                                <div class="single-blog-post style-2 d-flex align-items-center">
                                    <div class="post-thumbnail">
                                        <img src="img/bg-img/3.jpg" alt="">
                                    </div>
                                    <div class="post-content">
                                        <h6 class="post-title" > {{ $track->name ?? ''}}  </h6>
                                        <span> {{ $track->user->name ?? ''}} </span>
                                        <div class="post-meta d-flex justify-content-between">
                                            <span><i class="fa fa-comments-o" aria-hidden="true"></i> 25</span>
                                            <span><i class="fa fa-eye" aria-hidden="true"></i> 11</span>
                                            <span><i class="fa fa-thumbs-o-up" aria-hidden="true"></i> 19</span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </li>

                        @empty
                        <li class="nav-item" style="width: 100%;">
                            <a class="nav-link active" id="post-1-tab" data-toggle="pill" href="#post-1" role="tab" aria-controls="post-1" aria-selected="true">
                                <div class="single-blog-post style-2 d-flex align-items-center">
                                    No Item found
                                </div>
                            </a>
                        </li>

                        @endforelse

                    </ul>
                </div>

                <div class="col-12 col-md-7 col-lg-8" v-if="track">
                    <div class="single-video-area">
                        <iframe :src="track.url" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                 <!--  -->
                </div>

            </div>
        </div>
    </section>
    <!-- ##### Hero Area End ##### -->


@endsection


@push('scripts')

    <script type="text/javascript">
        new Vue({
            el: "#home-el",

            data(){
                return {
                    track: null,
                }
            },

            methods:{
                getTrack(id){

                    axios.get("/ajax-track/" + id).then(res => {
                        this.track = res.data;
                        console.log(this.track);
                    });

                }

            },

            created(){

            }
        })
    </script>

@endpush