    <!-- ##### Header Area Start ##### -->
    <header class="header-area">
        <!-- Top Header Area -->
        <div class="top-header-area">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-6">
                        <!-- Breaking News Widget -->
                        <div class="breaking-news-area d-flex align-items-center">
                            <div class="news-title">
                                <p>Sermons:</p>
                            </div>
                            <div id="breakingNewsTicker" class="ticker">
                                <ul>
                                    <li><a href="/">{{ _user()->name ?? ''}}</a></li>
                                    <li><a href="/">Welcome {{ auth()->user()->name ?? ''}}.</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6">
                        <div class="top-meta-data d-flex align-items-center justify-content-end">
                            <!-- Top Social Info -->
                            <div class="top-social-info">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-pinterest"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                                <a href="#"><i class="fa fa-youtube-play"></i></a>
                            </div>
                            <!-- Top Search Area -->
                            <div class="top-search-area">
                                <form action="/" method="get">
                                    <input type="search" name="top-search" id="topSearch" placeholder="Search...">
                                    <button type="submit" class="btn"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </div>
                            @if( auth()->check() )
                            @else
                            <a href="/login" class="login-btn"><i class="fa fa-user" aria-hidden="true"></i></a>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Navbar Area -->
        <div class="vizew-main-menu" id="sticker">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">

                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="vizewNav">

                        <!-- Nav brand -->
                        <a href="/" class="nav-brand"><img src="{{ _user()->image ?? asset('img/core-img/logo.png') }}" alt=""></a>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <div class="classy-menu">

                            <!-- Close Button -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li class="active"><a href="/">Home</a></li>
                                    @auth
                                    @if( auth()->user()->isSuperAdmin() )
                                    <li><a href="#">Access Control</a>
                                        <ul class="dropdown">
                                            <li><a href="/users">- Users</a></li>
                                            <li><a href="/roles">- Roles</a></li>
                                            <li><a href="/churches">- Churches</a></li>
                                            <li><a href="/categories">- Category</a></li>
                                            <li><a href="/tracks">- Tracks</a></li>
                                            <li><a href="/statuses">- Status</a></li>
                                            <li>
                                                <form method="POST" action="/logout" id="logout">@csrf</form>
                                                <a href="/" onclick="logout.submit()">- Logout</a>
                                            </li>
                                        </ul>
                                    </li>
                                    @endif
                                    @endauth                           

                                    <li><a href="#">People</a>
                                        <ul class="dropdown">
                                            @foreach( App\Models\User::get() as $user)
                                            <li><a href="/set/{{ $user->id }}">- {{ $user->name ?? ''}} </a></li>
                                            @endforeach
                                        </ul>
                                    </li>
<!--                                     <li><a href="#">People</a>
                                        @php 
                                        $users = App\Models\User::get();
                                        @endphp
                                        <div class="megamenu">

                                            

                                            <ul class="single-mega cn-col-4">

                                                @foreach( $users as $user)

                                                <li><a href="/">- {{ $user->name ?? ''}}</a></li>

                                                @endforeach

                                            </ul>


                                        </div>
                                    </li> -->
                                    <li><a href="/">Contact</a></li>
                                </ul>
                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <!-- ##### Header Area End ##### -->