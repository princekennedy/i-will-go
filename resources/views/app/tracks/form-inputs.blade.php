@php $editing = isset($track) @endphp

<div class="row">
    
    @if( $editing)
    <div class="col-sm-12">
        <div class="fileuploader">Upload</div>
    </div>
    @endif

    <x-inputs.group class="col-sm-12 col-lg-6">
        <x-inputs.text
            name="name"
            label="Name"
            :value="old('name', ($editing ? $track->name : ''))"
            maxlength="255"
            placeholder="Name"
            required
        ></x-inputs.text>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-6">
        <x-inputs.select name="user_id" label="User">
            @php $selected = old('user_id', ($editing ? $track->user_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the User</option>
            @foreach($users as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-6">
        <x-inputs.select name="category_id" label="Category">
            @php $selected = old('category_id', ($editing ? $track->category_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Category</option>
            @foreach($categories as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

    <x-inputs.group class="col-sm-12 col-lg-6">
        <x-inputs.select name="status_id" label="Status">
            @php $selected = old('status_id', ($editing ? $track->status_id : '')) @endphp
            <option disabled {{ empty($selected) ? 'selected' : '' }}>Please select the Status</option>
            @foreach($statuses as $value => $label)
            <option value="{{ $value }}" {{ $selected == $value ? 'selected' : '' }} >{{ $label }}</option>
            @endforeach
        </x-inputs.select>
    </x-inputs.group>

<!--     <x-inputs.group class="col-sm-12">
        <x-inputs.partials.label
            name="name"
            label="Name"
        ></x-inputs.partials.label
        ><br />

        <input type="file" name="media" id="name" class="form-control-file" />

        @if($editing && $track->name)
        <div class="mt-2">
            <a href="{{ $track->url }}" target="_blank"
                ><i class="icon ion-md-download"></i>&nbsp;Download</a
            >
        </div>
        @endif @error('name') @include('components.inputs.partials.error')
        @enderror
    </x-inputs.group>
 -->
    <x-inputs.group class="col-sm-12">
        <x-inputs.textarea
            name="description"
            label="Description"
            maxlength="255"
            >{{ old('description', ($editing ? $track->description : ''))
            }}</x-inputs.textarea
        >
    </x-inputs.group>

</div>

@push('scripts')
    
    <script type="text/javascript">
        $(document).ready(function(){

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
  
            $(".fileuploader").uploadFile({
                url:"/upload/{{ $track->id ?? ''}}",
                fileName:"media"
            });

        });  
    </script>

@endpush
