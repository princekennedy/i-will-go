@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('churches.index') }}" class="mr-4"
                    ><i class="icon ion-md-arrow-back"></i
                ></a>
                @lang('crud.churches.show_title')
            </h4>

            <div class="mt-4">
                <div class="mb-4">
                    <h5>@lang('crud.churches.inputs.name')</h5>
                    <span>{{ $church->name ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.churches.inputs.status_id')</h5>
                    <span>{{ optional($church->status)->name ?? '-' }}</span>
                </div>
                <div class="mb-4">
                    <h5>@lang('crud.churches.inputs.description')</h5>
                    <span>{{ $church->description ?? '-' }}</span>
                </div>
            </div>

            <div class="mt-4">
                <a href="{{ route('churches.index') }}" class="btn btn-light">
                    <i class="icon ion-md-return-left"></i>
                    @lang('crud.common.back')
                </a>

                @can('create', App\Models\Church::class)
                <a href="{{ route('churches.create') }}" class="btn btn-light">
                    <i class="icon ion-md-add"></i> @lang('crud.common.create')
                </a>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection
