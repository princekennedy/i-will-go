<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Status;
use App\Models\Category;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StatusCategoriesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_status_categories()
    {
        $status = Status::factory()->create();
        $categories = Category::factory()
            ->count(2)
            ->create([
                'status_id' => $status->id,
            ]);

        $response = $this->getJson(
            route('api.statuses.categories.index', $status)
        );

        $response->assertOk()->assertSee($categories[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_status_categories()
    {
        $status = Status::factory()->create();
        $data = Category::factory()
            ->make([
                'status_id' => $status->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.statuses.categories.store', $status),
            $data
        );

        unset($data['created_by']);
        unset($data['updated_by']);

        $this->assertDatabaseHas('categories', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $category = Category::latest('id')->first();

        $this->assertEquals($status->id, $category->status_id);
    }
}
