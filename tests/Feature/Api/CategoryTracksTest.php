<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Track;
use App\Models\Category;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryTracksTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_category_tracks()
    {
        $category = Category::factory()->create();
        $tracks = Track::factory()
            ->count(2)
            ->create([
                'category_id' => $category->id,
            ]);

        $response = $this->getJson(
            route('api.categories.tracks.index', $category)
        );

        $response->assertOk()->assertSee($tracks[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_category_tracks()
    {
        $category = Category::factory()->create();
        $data = Track::factory()
            ->make([
                'category_id' => $category->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.categories.tracks.store', $category),
            $data
        );

        unset($data['created_by']);
        unset($data['updated_by']);

        $this->assertDatabaseHas('tracks', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $track = Track::latest('id')->first();

        $this->assertEquals($category->id, $track->category_id);
    }
}
