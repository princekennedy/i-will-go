<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Track;

use App\Models\Status;
use App\Models\Category;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TrackTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_tracks_list()
    {
        $tracks = Track::factory()
            ->count(5)
            ->create();

        $response = $this->getJson(route('api.tracks.index'));

        $response->assertOk()->assertSee($tracks[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_track()
    {
        $data = Track::factory()
            ->make()
            ->toArray();

        $response = $this->postJson(route('api.tracks.store'), $data);

        unset($data['created_by']);
        unset($data['updated_by']);

        $this->assertDatabaseHas('tracks', $data);

        $response->assertStatus(201)->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_updates_the_track()
    {
        $track = Track::factory()->create();

        $user = User::factory()->create();
        $category = Category::factory()->create();
        $status = Status::factory()->create();
        $user = User::factory()->create();
        $user = User::factory()->create();

        $data = [
            'name' => $this->faker->name(),
            'description' => $this->faker->sentence(15),
            'user_id' => $user->id,
            'category_id' => $category->id,
            'status_id' => $status->id,
            'created_by' => $user->id,
            'updated_by' => $user->id,
        ];

        $response = $this->putJson(route('api.tracks.update', $track), $data);

        unset($data['created_by']);
        unset($data['updated_by']);

        $data['id'] = $track->id;

        $this->assertDatabaseHas('tracks', $data);

        $response->assertOk()->assertJsonFragment($data);
    }

    /**
     * @test
     */
    public function it_deletes_the_track()
    {
        $track = Track::factory()->create();

        $response = $this->deleteJson(route('api.tracks.destroy', $track));

        $this->assertSoftDeleted($track);

        $response->assertNoContent();
    }
}
