<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Track;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTracksTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_user_tracks()
    {
        $user = User::factory()->create();
        $tracks = Track::factory()
            ->count(2)
            ->create([
                'updated_by' => $user->id,
            ]);

        $response = $this->getJson(route('api.users.tracks.index', $user));

        $response->assertOk()->assertSee($tracks[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_user_tracks()
    {
        $user = User::factory()->create();
        $data = Track::factory()
            ->make([
                'updated_by' => $user->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.users.tracks.store', $user),
            $data
        );

        unset($data['created_by']);
        unset($data['updated_by']);

        $this->assertDatabaseHas('tracks', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $track = Track::latest('id')->first();

        $this->assertEquals($user->id, $track->updated_by);
    }
}
