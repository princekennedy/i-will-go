<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Status;
use App\Models\Church;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StatusChurchesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_status_churches()
    {
        $status = Status::factory()->create();
        $churches = Church::factory()
            ->count(2)
            ->create([
                'status_id' => $status->id,
            ]);

        $response = $this->getJson(
            route('api.statuses.churches.index', $status)
        );

        $response->assertOk()->assertSee($churches[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_status_churches()
    {
        $status = Status::factory()->create();
        $data = Church::factory()
            ->make([
                'status_id' => $status->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.statuses.churches.store', $status),
            $data
        );

        unset($data['created_by']);
        unset($data['updated_by']);

        $this->assertDatabaseHas('churches', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $church = Church::latest('id')->first();

        $this->assertEquals($status->id, $church->status_id);
    }
}
