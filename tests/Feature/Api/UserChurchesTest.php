<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Church;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserChurchesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_user_churches()
    {
        $user = User::factory()->create();
        $churches = Church::factory()
            ->count(2)
            ->create([
                'updated_by' => $user->id,
            ]);

        $response = $this->getJson(route('api.users.churches.index', $user));

        $response->assertOk()->assertSee($churches[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_user_churches()
    {
        $user = User::factory()->create();
        $data = Church::factory()
            ->make([
                'updated_by' => $user->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.users.churches.store', $user),
            $data
        );

        unset($data['created_by']);
        unset($data['updated_by']);

        $this->assertDatabaseHas('churches', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $church = Church::latest('id')->first();

        $this->assertEquals($user->id, $church->updated_by);
    }
}
