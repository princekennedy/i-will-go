<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Church;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ChurchUsersTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_church_users()
    {
        $church = Church::factory()->create();
        $users = User::factory()
            ->count(2)
            ->create([
                'church_id' => $church->id,
            ]);

        $response = $this->getJson(route('api.churches.users.index', $church));

        $response->assertOk()->assertSee($users[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_church_users()
    {
        $church = Church::factory()->create();
        $data = User::factory()
            ->make([
                'church_id' => $church->id,
            ])
            ->toArray();
        $data['password'] = \Str::random('8');

        $response = $this->postJson(
            route('api.churches.users.store', $church),
            $data
        );

        unset($data['password']);
        unset($data['email_verified_at']);
        unset($data['created_by']);
        unset($data['updated_by']);

        $this->assertDatabaseHas('users', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $user = User::latest('id')->first();

        $this->assertEquals($church->id, $user->church_id);
    }
}
