<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Track;
use App\Models\Status;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StatusTracksTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_status_tracks()
    {
        $status = Status::factory()->create();
        $tracks = Track::factory()
            ->count(2)
            ->create([
                'status_id' => $status->id,
            ]);

        $response = $this->getJson(route('api.statuses.tracks.index', $status));

        $response->assertOk()->assertSee($tracks[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_status_tracks()
    {
        $status = Status::factory()->create();
        $data = Track::factory()
            ->make([
                'status_id' => $status->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.statuses.tracks.store', $status),
            $data
        );

        unset($data['created_by']);
        unset($data['updated_by']);

        $this->assertDatabaseHas('tracks', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $track = Track::latest('id')->first();

        $this->assertEquals($status->id, $track->status_id);
    }
}
