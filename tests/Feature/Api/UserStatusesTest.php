<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Models\Status;

use Tests\TestCase;
use Laravel\Sanctum\Sanctum;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserStatusesTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $user = User::factory()->create(['email' => 'admin@admin.com']);

        Sanctum::actingAs($user, [], 'web');

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_gets_user_statuses()
    {
        $user = User::factory()->create();
        $statuses = Status::factory()
            ->count(2)
            ->create([
                'updated_by' => $user->id,
            ]);

        $response = $this->getJson(route('api.users.statuses.index', $user));

        $response->assertOk()->assertSee($statuses[0]->name);
    }

    /**
     * @test
     */
    public function it_stores_the_user_statuses()
    {
        $user = User::factory()->create();
        $data = Status::factory()
            ->make([
                'updated_by' => $user->id,
            ])
            ->toArray();

        $response = $this->postJson(
            route('api.users.statuses.store', $user),
            $data
        );

        unset($data['created_by']);
        unset($data['updated_by']);

        $this->assertDatabaseHas('statuses', $data);

        $response->assertStatus(201)->assertJsonFragment($data);

        $status = Status::latest('id')->first();

        $this->assertEquals($user->id, $status->updated_by);
    }
}
