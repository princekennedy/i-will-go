<?php

namespace Tests\Feature\Controllers;

use App\Models\User;
use App\Models\Track;

use App\Models\Status;
use App\Models\Category;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TrackControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->actingAs(
            User::factory()->create(['email' => 'admin@admin.com'])
        );

        $this->seed(\Database\Seeders\PermissionsSeeder::class);

        $this->withoutExceptionHandling();
    }

    /**
     * @test
     */
    public function it_displays_index_view_with_tracks()
    {
        $tracks = Track::factory()
            ->count(5)
            ->create();

        $response = $this->get(route('tracks.index'));

        $response
            ->assertOk()
            ->assertViewIs('app.tracks.index')
            ->assertViewHas('tracks');
    }

    /**
     * @test
     */
    public function it_displays_create_view_for_track()
    {
        $response = $this->get(route('tracks.create'));

        $response->assertOk()->assertViewIs('app.tracks.create');
    }

    /**
     * @test
     */
    public function it_stores_the_track()
    {
        $data = Track::factory()
            ->make()
            ->toArray();

        $response = $this->post(route('tracks.store'), $data);

        unset($data['created_by']);
        unset($data['updated_by']);

        $this->assertDatabaseHas('tracks', $data);

        $track = Track::latest('id')->first();

        $response->assertRedirect(route('tracks.edit', $track));
    }

    /**
     * @test
     */
    public function it_displays_show_view_for_track()
    {
        $track = Track::factory()->create();

        $response = $this->get(route('tracks.show', $track));

        $response
            ->assertOk()
            ->assertViewIs('app.tracks.show')
            ->assertViewHas('track');
    }

    /**
     * @test
     */
    public function it_displays_edit_view_for_track()
    {
        $track = Track::factory()->create();

        $response = $this->get(route('tracks.edit', $track));

        $response
            ->assertOk()
            ->assertViewIs('app.tracks.edit')
            ->assertViewHas('track');
    }

    /**
     * @test
     */
    public function it_updates_the_track()
    {
        $track = Track::factory()->create();

        $user = User::factory()->create();
        $category = Category::factory()->create();
        $status = Status::factory()->create();
        $user = User::factory()->create();
        $user = User::factory()->create();

        $data = [
            'name' => $this->faker->name(),
            'description' => $this->faker->sentence(15),
            'user_id' => $user->id,
            'category_id' => $category->id,
            'status_id' => $status->id,
            'created_by' => $user->id,
            'updated_by' => $user->id,
        ];

        $response = $this->put(route('tracks.update', $track), $data);

        unset($data['created_by']);
        unset($data['updated_by']);

        $data['id'] = $track->id;

        $this->assertDatabaseHas('tracks', $data);

        $response->assertRedirect(route('tracks.edit', $track));
    }

    /**
     * @test
     */
    public function it_deletes_the_track()
    {
        $track = Track::factory()->create();

        $response = $this->delete(route('tracks.destroy', $track));

        $response->assertRedirect(route('tracks.index'));

        $this->assertSoftDeleted($track);
    }
}
