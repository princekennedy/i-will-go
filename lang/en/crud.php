<?php

return [
    'common' => [
        'actions' => 'Actions',
        'create' => 'Create',
        'edit' => 'Edit',
        'update' => 'Update',
        'new' => 'New',
        'cancel' => 'Cancel',
        'attach' => 'Attach',
        'detach' => 'Detach',
        'save' => 'Save',
        'delete' => 'Delete',
        'delete_selected' => 'Delete selected',
        'search' => 'Search...',
        'back' => 'Back to Index',
        'are_you_sure' => 'Are you sure?',
        'no_items_found' => 'No items found',
        'created' => 'Successfully created',
        'saved' => 'Saved successfully',
        'removed' => 'Successfully removed',
    ],

    'categories' => [
        'name' => 'Categories',
        'index_title' => 'Categories List',
        'new_title' => 'New Category',
        'create_title' => 'Create Category',
        'edit_title' => 'Edit Category',
        'show_title' => 'Show Category',
        'inputs' => [
            'name' => 'Name',
            'status_id' => 'Status',
            'description' => 'Description',
        ],
    ],

    'churches' => [
        'name' => 'Churches',
        'index_title' => 'Churches List',
        'new_title' => 'New Church',
        'create_title' => 'Create Church',
        'edit_title' => 'Edit Church',
        'show_title' => 'Show Church',
        'inputs' => [
            'name' => 'Name',
            'status_id' => 'Status',
            'description' => 'Description',
        ],
    ],

    'statuses' => [
        'name' => 'Statuses',
        'index_title' => 'Statuses List',
        'new_title' => 'New Status',
        'create_title' => 'Create Status',
        'edit_title' => 'Edit Status',
        'show_title' => 'Show Status',
        'inputs' => [
            'name' => 'Name',
            'color' => 'Color',
            'description' => 'Description',
        ],
    ],

    'tracks' => [
        'name' => 'Tracks',
        'index_title' => 'Tracks List',
        'new_title' => 'New Track',
        'create_title' => 'Create Track',
        'edit_title' => 'Edit Track',
        'show_title' => 'Show Track',
        'inputs' => [
            'name' => 'Name',
            'user_id' => 'User',
            'category_id' => 'Category',
            'status_id' => 'Status',
            'name' => 'Name',
            'description' => 'Description',
        ],
    ],

    'users' => [
        'name' => 'Users',
        'index_title' => 'Users List',
        'new_title' => 'New User',
        'create_title' => 'Create User',
        'edit_title' => 'Edit User',
        'show_title' => 'Show User',
        'inputs' => [
            'name' => 'Name',
            'email' => 'Email',
            'title' => 'Title',
            'phone' => 'Phone',
            'password' => 'Password',
            'status_id' => 'Status',
            'church_id' => 'Church',
            'description' => 'Description',
            'name' => 'Name',
        ],
    ],

    'roles' => [
        'name' => 'Roles',
        'index_title' => 'Roles List',
        'create_title' => 'Create Role',
        'edit_title' => 'Edit Role',
        'show_title' => 'Show Role',
        'inputs' => [
            'name' => 'Name',
        ],
    ],

    'permissions' => [
        'name' => 'Permissions',
        'index_title' => 'Permissions List',
        'create_title' => 'Create Permission',
        'edit_title' => 'Edit Permission',
        'show_title' => 'Show Permission',
        'inputs' => [
            'name' => 'Name',
        ],
    ],
];
