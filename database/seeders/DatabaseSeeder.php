<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Status::create(['id' => 1, 'name' => 'Active']);
        \App\Models\Status::create(['id' => 2, 'name' => 'Inactive']);


        // Adding an admin user
        $user = \App\Models\User::factory()
            ->count(1)
            ->create([
                'email' => 'admin@admin.com',
                'password' => \Hash::make('admin'),
                'status_id' => 1,
            ]);
        $this->call(PermissionsSeeder::class);

        // GENERAL
        \App\Models\Status::create(['id' => 10, 'name' => 'Pending']);
        \App\Models\Status::create(['id' => 11, 'name' => 'Approved']);
        
        // $this->call(CategorySeeder::class);
        // $this->call(ChurchSeeder::class);
        // $this->call(StatusSeeder::class);
        // $this->call(TrackSeeder::class);
        // $this->call(UserSeeder::class);
    }
}
